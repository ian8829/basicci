<?php

class R2u_user_model extends CI_Model {

  public function get_results() {

    // $this->db->where([
    //   'id' => $user_id,
    //   'username' => $username
    //   ]);

    // $query = $this->db->get('users');

    $query = "
    SELECT 
      account_seq, 
      name, 
      COUNT(account_seq) AS product_count,
      email
    FROM `MY_PRODUCTS`
    JOIN `ACCOUNTS`
      ON account_seq = `ACCOUNTS`.seq
    WHERE expired_at BETWEEN DATE_ADD(CURDATE(), INTERVAL 5 day) AND DATE_ADD(CURDATE(), INTERVAL 6 day)
    AND `MY_PRODUCTS`.site_seq = 1 && `MY_PRODUCTS`.state = 1
    GROUP BY account_seq
    ORDER BY account_seq;
    ";
  
    $result = $this->db->query($query)->result();
    return $result;
  }

  public function get_dday5_count()
  {
    $query = "
    SELECT 
      MY_PRODUCTS.account_seq, 
      ACCOUNTS.name, 
      COUNT(MY_PRODUCTS.account_seq) AS product_count,
      ACCOUNTS.email,
      ACCOUNTS.phone_number
    FROM MY_PRODUCTS
    JOIN ACCOUNTS
      ON MY_PRODUCTS.account_seq = ACCOUNTS.seq
    WHERE MY_PRODUCTS.expired_at BETWEEN DATE_ADD(CURDATE(), INTERVAL 5 day) AND DATE_ADD(CURDATE(), INTERVAL 6 day)
    AND MY_PRODUCTS.site_seq = 1 && MY_PRODUCTS.state = 1
    GROUP BY account_seq
    ORDER BY account_seq;
    ";

    $result = $this->db->query($query)->result();

    return $result;
  }
}

?>