<?php

class R2u_users extends CI_Controller {

  public function show() {
    $data['result'] = $this->r2u_user_model->get_results();

    $this->load->view('r2u_product_view', $data);
  }

  public function showCount() {
    $data['count'] = $this->r2u_user_model->get_dday5_count();

    $this->load->view('r2u_count_view', $data);
  }

}

?>